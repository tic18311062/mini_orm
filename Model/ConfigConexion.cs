
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// @author Alonso Lopez
/// </summary>
public class ConfigConexion {

	public ConfigConexion() {
	}

	public static string dbServer;

	public static string dbUs;

	public static string dbPwd;

	public static string dbDBName;

	public static string dbPort;

	/// <summary>
	/// Enum definido para seleccionar el tipo de base de dstos.  SQL_SERVER, MYSQL, ORACLE_XE, PGSQL
	/// </summary>
	public static DBMS dbms = MYSQL;

	public IBD conexion;

	public static string errorMsg;

	public IBD compone;


}