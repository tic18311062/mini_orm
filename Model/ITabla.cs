
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public interface ITabla {


	public abstract void crearTabla(nombreTabla:string, campos: List<Campo>, restricciones: List<Restriccion>)();

	public abstract void borrarTabla(tabla:string)();

	public abstract void limpiarTabla(tabla:string)();

}